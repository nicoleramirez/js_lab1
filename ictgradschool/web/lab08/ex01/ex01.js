"use strict";

// TODO Declare variables here
var myFirstNumber = 3;
console.log(myFirstNumber);

var myFirstBoolean = false;
console.log(myFirstBoolean);

var mySecondNumber = 10.25;
console.log(mySecondNumber);

var myFirstString = "Hello";
console.log(myFirstString);

var mySecondString = "World";
console.log(mySecondString);

var mySecondNumber = myFirstNumber + 7;
console.log(mySecondNumber);

var myThirdNumber = mySecondNumber - myFirstNumber; /*basically cuts the numbers to the right of the decimal point off, doesnt round down or up*/
console.log(myThirdNumber);

var thirdString = myFirstString + mySecondString;
console.log(thirdString);

var stringNumber = myFirstString + myFirstNumber;
console.log(stringNumber);

var stringNumber2 = mySecondString - mySecondNumber;
console.log(stringNumber2);


// TODO Use console.log statements to print the values of the variables to the command line.

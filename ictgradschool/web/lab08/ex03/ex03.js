"use strict";

// Provided variables
var string1 = "Hello World";
var string2 = "Hi everybody!"
var string3 = "Hi, Dr Nick!"

// TODO Your answers here.

var charNumber = string1.length;
console.log("Number of characters: " + charNumber);

var substringStr1 = string1.substring(8,11);
console.log("The last 3 characters of string1 is: " + substringStr1);

var spaceTest = string2.includes(" ");
console.log("Does it contain spaces? " + spaceTest);

var lastChar = string3.charAt(string3.length-1);
console.log("Last Character of String 3: " + lastChar);





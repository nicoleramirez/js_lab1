"use strict";

// Provided variables
var amount = 3;
var userResponse = 'Y';
var firstName = "Bob";
var singer = "NOT Taylor Swift!";
var year = 1977;

// Example
var isOdd = (amount % 2 != 0);
console.log("isOdd = " + isOdd);

// TODO write your answers here

var userResponseTest = (userResponse == "Y") || (userResponse == "y")
console.log("Is it equal to Y or y? = " + userResponseTest);

var beginsWithA = firstName.charAt(0) == "A";
console.log("Does it begin with A? = " + beginsWithA);

var isTaylorSwift = singer == "Taylor Swift";
console.log("Is the singer Taylor Swift? " + isTaylorSwift);

var yearBornTest = (year > 1978) && (year != 2013);
console.log("Is the year greater than 1978 but not equal to 2013? " + yearBornTest);


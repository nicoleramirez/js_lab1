"use strict";

// Provided variables.
var code = "F";

// Variables you'll be assigning to in this question.
var gender;

// TODO Your code for part (1) here.
if (code == "F" || code == "f"){
    gender = "Female";
}

if (code == "M" || code == "m"){
    gender = "Male";
}

// Printing the answer
console.log("Part 1: The gender is: " + gender)